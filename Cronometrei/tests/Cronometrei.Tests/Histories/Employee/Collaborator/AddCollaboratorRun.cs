﻿using Cronometrei.Histories.EmployeeHistory.Collaborator;
using Cronometrei.Support.Dtos.CollaboratorDtos;
using Cronometrei.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Cronometrei.Tests.Histories.Employee.Collaborator
{
    public class AddCollaboratorRun
    {
        [Fact]
        public async Task DeveCadastrarColaborador()
        {
            // Arrange
            var contextFake = await ContextFake.Gerar(nomeDoBancoDeDados: "dbTesteCollaborator");

            var collaboratorDto = new PostCollaboratorDto
            {
                Name = "Alan",
                Registration = "123456",
                BirthDate = new DateTime(1997, 10, 26),
                Login = "alan",
                Password = "alan"
            };

            var addCollaborator = new AddCollaborator(contextFake);

            // Act
            await addCollaborator.Run(collaboratorDto);

            var employee = await contextFake.Employees.Include(employee => employee.User)
                                                      .FirstOrDefaultAsync(employee =>
                                                          employee.User.Login.ToLower() == collaboratorDto.Login
                                                          && employee.User.Password == collaboratorDto.Password
                                                      );

            // Assert
            Assert.Equal(collaboratorDto.Name, employee.Name);
            Assert.Equal(collaboratorDto.Registration, employee.Registration);
            Assert.Equal(collaboratorDto.BirthDate, employee.BirthDate);
            Assert.Equal(collaboratorDto.Login, employee.User.Login);
            Assert.Equal(collaboratorDto.Password, employee.User.Password);
        }
    }
}
