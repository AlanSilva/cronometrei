﻿using Cronometrei.Domain.Models;
using System;
using Xunit;

namespace Cronometrei.Tests.Models
{
    public class EmployeeConstructor
    {
        [Fact]
        public void DeveRetornarObjetoConstruido()
        {
            // Arrange
            var id = Guid.NewGuid();

            var name = "Alan";

            var registration = "12345789";

            var birthDate = new DateTime(1997, 10, 26);

            var login = "alan";

            var password = "alan";

            var role = "Manager";

            var user = new User(id, login, password, role);

            // Act
            var employee = new Employee(id, name, registration, birthDate, user);

            // Assert
            Assert.Equal(id, employee.Id);
            Assert.Equal(name, employee.Name);
            Assert.Equal(registration, employee.Registration);
            Assert.Equal(birthDate, employee.BirthDate);
            Assert.True(employee.Status);
            Assert.Equal(user.Id, employee.User.Id);
            Assert.Equal(user.Login, employee.User.Login);
            Assert.Equal(user.Password, employee.User.Password);
        }
    }
}
