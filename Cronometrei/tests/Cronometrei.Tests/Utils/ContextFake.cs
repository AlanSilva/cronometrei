﻿using Cronometrei.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Cronometrei.Tests.Utils
{
    class ContextFake
    {
        public static async Task<CronometreiContext> Gerar(string nomeDoBancoDeDados)
        {
            var options = new DbContextOptionsBuilder<CronometreiContext>().UseInMemoryDatabase(databaseName: nomeDoBancoDeDados).Options;

            var contexto = new CronometreiContext(options);

            return await Task.FromResult(contexto);
        }
    }
}
