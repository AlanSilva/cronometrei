﻿namespace Cronometrei.Domain.Enums
{
    public enum Role
    {
        Manager = 1,
        Collaborator
    }
}
