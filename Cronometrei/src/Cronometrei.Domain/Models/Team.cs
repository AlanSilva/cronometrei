﻿using System;
using System.Collections.Generic;

namespace Cronometrei.Domain.Models
{
    public class Team
    {
        private Team() { }

        public Team(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
            Status = true;
            CreatedAt = DateTime.Now;
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public bool Status { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public List<EmployeeTeam> EmployeeTeams { get; private set; } = new List<EmployeeTeam>();

        public List<Project> Projects { get; private set; } = new List<Project>();
    }
}
