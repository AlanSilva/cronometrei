﻿using System;
using System.Collections.Generic;

namespace Cronometrei.Domain.Models
{
    public class Employee
    {
        private Employee() { }

        public Employee(Guid id, string name, string registration, DateTime birthDate, User user)
        {
            Id = id;
            Name = name;
            Registration = registration;
            BirthDate = birthDate;
            Status = true;
            User = user;
        }

        public Guid Id { get; private set; }

        public string Name { get; private set; }

        public string Registration { get; private set; }

        public DateTime BirthDate { get; private set; }

        public bool Status { get; private set; }

        public User User { get; private set; }

        public List<EmployeeTeam> EmployeeTeams { get; private set; } = new List<EmployeeTeam>();

        public List<WorkedHour> WorkedHours { get; private set; } = new List<WorkedHour>();
    }
}
