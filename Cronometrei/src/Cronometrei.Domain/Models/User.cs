﻿using System;

namespace Cronometrei.Domain.Models
{
    public class User
    {
        public User(Guid id, string login, string password, string role)
        {
            Id = id;
            Login = login;
            Password = password;
            Role = role;
        }

        public Guid Id { get; private set; }

        public string Login { get; private set; }

        public string Password { get; private set; }

        public string Role { get; private set; }

        public Employee Employee { get; private set; }

        public void SetPasswordToEmpty()
        {
            Password = "";
        }
    }
}
