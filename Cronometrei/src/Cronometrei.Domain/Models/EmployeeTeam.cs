﻿using System;

namespace Cronometrei.Domain.Models
{
    public class EmployeeTeam
    {
        private EmployeeTeam() { }

        public EmployeeTeam(Guid employeeId, Guid teamId)
        {
            EmployeeId = employeeId;
            TeamId = teamId;
        }

        public Guid EmployeeId { get; private set; }

        public Employee Employee { get; private set; }

        public Guid TeamId { get; private set; }

        public Team Team { get; private set; }
    }
}
