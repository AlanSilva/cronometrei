﻿using System;

namespace Cronometrei.Domain.Models
{
    public class WorkedHour
    {
        private WorkedHour() { }

        public WorkedHour(Guid employeeId, Guid projectId, int hours, int minutes)
        {
            Id = Guid.NewGuid();
            EmployeeId = employeeId;
            ProjectId = projectId;
            Hours = hours;
            Minutes = minutes;
            RegisteredAt = DateTime.Now;
        }

        public Guid Id { get; private set; }

        public Guid EmployeeId { get; private set; }

        public Employee Employee { get; private set; }

        public Guid ProjectId { get; private set; }

        public Project Project { get; private set; }

        public int Hours { get; private set; }

        public int Minutes { get; private set; }

        public DateTime RegisteredAt { get; private set; }
    }
}
