﻿using System;
using System.Collections.Generic;

namespace Cronometrei.Domain.Models
{
    public class Project
    {
        private Project() { }

        public Project(string name, string description)
        {
            Id = Guid.NewGuid();
            Name = name;
            Description = description;
            Status = true;
            CreatedAt = DateTime.Now;
        }

        public Guid Id { get; private set; }

        public Guid? TeamId { get; private set; }

        public Team Team { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public bool Status { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public List<WorkedHour> WorkedHours { get; private set; } = new List<WorkedHour>();

        public void SetTeam(Guid teamId)
        {
            TeamId = teamId;
        }
    }
}
