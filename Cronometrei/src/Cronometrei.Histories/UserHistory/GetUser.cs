﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Cronometrei.Histories.UserHistory
{
    public class GetUser
    {
        private readonly CronometreiContext _context;

        public GetUser(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<User> Run(string login, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(user => user.Login.ToLower() == login.ToLower() && user.Password == password);

            return user;
        }
    }
}
