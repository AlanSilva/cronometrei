﻿using Cronometrei.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Cronometrei.Histories.UserHistory
{
    public class CheckIfUserAlreadyExistsByLogin
    {
        private readonly CronometreiContext _context;

        public CheckIfUserAlreadyExistsByLogin(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<bool> Run(string login)
        {
            var user = await _context.Users.FirstOrDefaultAsync(user => user.Login.ToLower() == login.ToLower());

            if (user != null)
                return true;

            return false;
        }
    }
}
