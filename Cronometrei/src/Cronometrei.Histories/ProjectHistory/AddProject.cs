﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.ProjectDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.ProjectHistory
{
    public class AddProject
    {
        private readonly CronometreiContext _context;

        public AddProject(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostProjectDto projectDto)
        {
            Project project = projectDto;

            await _context.AddAsync(project);

            await _context.SaveChangesAsync();
        }
    }
}
