﻿using Cronometrei.Data.Context;
using Cronometrei.Support.Dtos.ProjectDtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cronometrei.Histories.ProjectHistory
{
    public class ListProjects
    {
        private readonly CronometreiContext _context;

        public ListProjects(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<List<GetProjectDto>> Run()
        {
            var projects = await _context.Projects.Include(project => project.Team)
                                                  .Include(project => project.WorkedHours)
                                                  .ToListAsync();

            var projectsDto = projects.Select(project =>
                                      {
                                          GetProjectDto projectDto = project;

                                          return projectDto;
                                      })
                                      .ToList();

            return projectsDto;
        }
    }
}
