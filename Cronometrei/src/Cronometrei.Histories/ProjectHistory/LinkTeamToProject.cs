﻿using Cronometrei.Data.Context;
using Cronometrei.Support.Dtos.ProjectDtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.ProjectHistory
{
    public class LinkTeamToProject
    {
        private readonly CronometreiContext _context;

        public LinkTeamToProject(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostLinkTeamToProjectDto linkTeamToProjectDto)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(project => project.Id == linkTeamToProjectDto.ProjectId);

            project.SetTeam(linkTeamToProjectDto.TeamId);

            _context.Update(project);

            await _context.SaveChangesAsync();
        }
    }
}
