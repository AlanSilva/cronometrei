﻿using Cronometrei.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.ProjectHistory
{
    public class CheckIfTheProjectExists
    {
        private readonly CronometreiContext _context;

        public CheckIfTheProjectExists(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<bool> Run(Guid projectId)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(project => project.Id == projectId);

            if (project != null)
                return true;

            return false;
        }
    }
}
