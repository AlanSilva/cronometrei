﻿using Cronometrei.Data.Context;
using Cronometrei.Support.Dtos.ProjectDtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.ProjectHistory
{
    public class GetProjectById
    {
        private readonly CronometreiContext _context;

        public GetProjectById(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<GetProjectDto> Run(Guid id)
        {
            var project = await _context.Projects.Include(project => project.Team)
                                                 .Include(project => project.WorkedHours)
                                                 .FirstOrDefaultAsync(project => project.Id == id);

            if (project is null)
                return null;

            GetProjectDto projectDto = project;

            return projectDto;
        }
    }
}
