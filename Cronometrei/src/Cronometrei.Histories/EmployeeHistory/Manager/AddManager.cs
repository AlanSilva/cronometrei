﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.ManagerDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.EmployeeHistory.Manager
{
    public class AddManager
    {
        private readonly CronometreiContext _context;

        public AddManager(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostManagerDto managerDto)
        {
            Employee employee = managerDto;

            await _context.AddAsync(employee);

            await _context.SaveChangesAsync();
        }
    }
}
