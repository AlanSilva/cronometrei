﻿using Cronometrei.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.EmployeeHistory.Collaborator
{
    public class CheckIfTheCollaboratorExists
    {
        private readonly CronometreiContext _context;

        public CheckIfTheCollaboratorExists(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<bool> Run(Guid employeeId)
        {
            var collaborator = await _context.Employees.FirstOrDefaultAsync(employee => employee.Id == employeeId);

            if (collaborator != null)
                return true;

            return false;
        }
    }
}
