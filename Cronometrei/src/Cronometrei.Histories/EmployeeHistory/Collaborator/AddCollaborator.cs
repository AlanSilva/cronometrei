﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.CollaboratorDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.EmployeeHistory.Collaborator
{
    public class AddCollaborator
    {
        private readonly CronometreiContext _context;

        public AddCollaborator(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostCollaboratorDto collaborateDto)
        {
            Employee employee = collaborateDto;

            await _context.AddAsync(employee);

            await _context.SaveChangesAsync();
        }
    }
}
