﻿using Cronometrei.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.TeamHistory
{
    public class CheckIfTheTeamExists
    {
        private readonly CronometreiContext _context;

        public CheckIfTheTeamExists(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<bool> Run(Guid teamId)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(team => team.Id == teamId);

            if (team != null)
                return true;

            return false;
        }
    }
}
