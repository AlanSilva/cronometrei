﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.TeamDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.TeamHistory
{
    public class AddTeam
    {
        private readonly CronometreiContext _context;

        public AddTeam(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostTeamDto teamDto)
        {
            Team team = teamDto;

            await _context.AddAsync(team);

            await _context.SaveChangesAsync();
        }
    }
}
