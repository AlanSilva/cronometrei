﻿using Cronometrei.Data.Context;
using Cronometrei.Support.Dtos.TeamDtos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Histories.TeamHistory
{
    public class GetTeamById
    {
        private readonly CronometreiContext _context;

        public GetTeamById(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<GetTeamDto> Run(Guid id)
        {
            var team = await _context.Teams.Include(team => team.EmployeeTeams)
                                               .ThenInclude(employeeTeam => employeeTeam.Employee)
                                                   .ThenInclude(employee => employee.WorkedHours)
                                           .FirstOrDefaultAsync(team => team.Id == id);

            if (team is null)
                return null;

            GetTeamDto teamDto = team;

            return teamDto;
        }
    }
}
