﻿using Cronometrei.Data.Context;
using Cronometrei.Support.Dtos.TeamDtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cronometrei.Histories.TeamHistory
{
    public class ListTeams
    {
        private readonly CronometreiContext _context;

        public ListTeams(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<List<GetTeamDto>> Run()
        {
            var teams = await _context.Teams.Include(team => team.EmployeeTeams)
                                                .ThenInclude(employeeTeam => employeeTeam.Employee)
                                                    .ThenInclude(employee => employee.WorkedHours)
                                            .ToListAsync();

            var teamsDto = teams.Select(team =>
                                {
                                    GetTeamDto teamDto = team;

                                    return teamDto;
                                })
                                .ToList();

            return teamsDto;
        }
    }
}
