﻿using Cronometrei.Histories.EmployeeHistory.Collaborator;
using Cronometrei.Histories.EmployeeHistory.Manager;
using Cronometrei.Histories.EmployeeTeamHistory;
using Cronometrei.Histories.ProjectHistory;
using Cronometrei.Histories.TeamHistory;
using Cronometrei.Histories.UserHistory;
using Cronometrei.Histories.WorkedHourHistory;
using Cronometrei.Support.Dtos.CollaboratorDtos;
using Cronometrei.Support.Dtos.EmployeeTeamDtos;
using Cronometrei.Support.Dtos.ManagerDtos;
using Cronometrei.Support.Dtos.ProjectDtos;
using Cronometrei.Support.Dtos.TeamDtos;
using Cronometrei.Support.Dtos.UserDtos;
using Cronometrei.Support.Dtos.WorkedHourDtos;
using Cronometrei.Support.Validators.CollaboratorValidators;
using Cronometrei.Support.Validators.EmployeeTeamValidators;
using Cronometrei.Support.Validators.ManagerValidators;
using Cronometrei.Support.Validators.ProjectValidators;
using Cronometrei.Support.Validators.TeamValidators;
using Cronometrei.Support.Validators.UserValidators;
using Cronometrei.Support.Validators.WorkedHourValidators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace Cronometrei.Histories
{
    public static class Setup
    {
        // Dependency injection
        public static void AddHistories(this IServiceCollection services)
        {
            #region Histories
            // Employees - Collaborators
            services.AddScoped<AddCollaborator>();
            services.AddScoped<CheckIfTheCollaboratorExists>();

            // Employees = Managers
            services.AddScoped<AddManager>();

            // Employee Teams
            services.AddScoped<AddCollaboratorToTeam>();
            services.AddScoped<CheckIfTheCollaboratorIsAlreadyOnTheTeam>();

            // Projects
            services.AddScoped<AddProject>();
            services.AddScoped<CheckIfTheProjectExists>();
            services.AddScoped<GetProjectById>();
            services.AddScoped<LinkTeamToProject>();
            services.AddScoped<ListProjects>();

            // Teams
            services.AddScoped<AddTeam>();
            services.AddScoped<CheckIfTheTeamExists>();
            services.AddScoped<GetTeamById>();
            services.AddScoped<ListTeams>();

            // Users
            services.AddScoped<CheckIfUserAlreadyExistsByLogin>();
            services.AddScoped<GetUser>();

            // Worked Hours
            services.AddScoped<RegisterHoursWorked>();
            #endregion

            #region Dto Validators
            // Employee - Collaborators
            services.AddTransient<IValidator<PostCollaboratorDto>, PostCollaboratorDtoValidator>();

            // Employee - Managers
            services.AddTransient<IValidator<PostManagerDto>, PostManagerDtoValidator>();

            // Employee Teams
            services.AddTransient<IValidator<PostEmployeeTeamDto>, PostEmployeeTeamDtoValidator>();

            // Projects
            services.AddTransient<IValidator<PostLinkTeamToProjectDto>, PostLinkTeamToProjectDtoValidator>();
            services.AddTransient<IValidator<PostProjectDto>, PostProjectDtoValidator>();

            // Teams
            services.AddTransient<IValidator<PostTeamDto>, PostTeamDtoValidator>();

            // Users
            services.AddTransient<IValidator<PostUserAuthenticationDto>, PostUserAuthenticationDtoValidator>();

            // Worked Hours
            services.AddTransient<IValidator<PostWorkedHourDto>, PostWorkedHourDtoValidator>();
            #endregion
        }
    }
}
