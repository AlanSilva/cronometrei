﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.EmployeeTeamDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.EmployeeTeamHistory
{
    public class AddCollaboratorToTeam
    {
        private readonly CronometreiContext _context;

        public AddCollaboratorToTeam(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostEmployeeTeamDto employeeTeamDto)
        {
            EmployeeTeam employeeTeam = employeeTeamDto;

            await _context.AddAsync(employeeTeam);

            await _context.SaveChangesAsync();
        }
    }
}
