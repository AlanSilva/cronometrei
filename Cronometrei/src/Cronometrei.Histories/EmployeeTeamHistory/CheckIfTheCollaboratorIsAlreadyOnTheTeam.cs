﻿using Cronometrei.Data.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cronometrei.Histories.EmployeeTeamHistory
{
    public class CheckIfTheCollaboratorIsAlreadyOnTheTeam
    {
        private readonly CronometreiContext _context;

        public CheckIfTheCollaboratorIsAlreadyOnTheTeam(CronometreiContext context)
        {
            _context = context;
        }

        public async Task<bool> Run(Guid employeeId, Guid teamId)
        {
            var employeeTeam = _context.EmployeeTeams.FirstOrDefault(employeeTeam => employeeTeam.EmployeeId == employeeId && employeeTeam.TeamId == teamId);

            if (employeeTeam != null)
                return true;

            return false;
        }
    }
}
