﻿using Cronometrei.Data.Context;
using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.WorkedHourDtos;
using System.Threading.Tasks;

namespace Cronometrei.Histories.WorkedHourHistory
{
    public class RegisterHoursWorked
    {
        private readonly CronometreiContext _context;

        public RegisterHoursWorked(CronometreiContext context)
        {
            _context = context;
        }

        public async Task Run(PostWorkedHourDto workedHourDto)
        {
            WorkedHour workedHour = workedHourDto;

            await _context.AddAsync(workedHour);

            await _context.SaveChangesAsync();
        }
    }
}
