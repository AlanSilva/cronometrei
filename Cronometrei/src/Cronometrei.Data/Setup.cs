﻿using Cronometrei.Data.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cronometrei.Data
{
    public static class Setup
    {
        public static void AddContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CronometreiContext>(x =>
                x.EnableSensitiveDataLogging()
                 .UseSqlServer(connectionString: configuration.GetConnectionString("CronometreiContext"), sqlServerOptionsAction: sqlOptions => sqlOptions.EnableRetryOnFailure())
            );
        }

        public static void MigrationInitialization(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                serviceScope.ServiceProvider.GetService<CronometreiContext>().Database.Migrate();
            }
        }
    }
}
