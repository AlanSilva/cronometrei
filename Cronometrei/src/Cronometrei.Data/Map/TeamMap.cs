﻿using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cronometrei.Data.Map
{
    public class TeamMap : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(team => team.Name)
                   .HasColumnType("varchar(50)")
                   .IsRequired();
        }
    }
}
