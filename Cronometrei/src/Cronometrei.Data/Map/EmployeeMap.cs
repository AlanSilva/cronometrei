﻿using Cronometrei.Domain.Enums;
using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Cronometrei.Data.Map
{
    public class EmployeeMap : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(employee => employee.Name)
                   .IsRequired();

            builder.Property(employee => employee.Registration)
                   .IsRequired();

            builder.HasOne(employee => employee.User)
                   .WithOne(user => user.Employee)
                   .HasForeignKey<User>(user => user.Id);
        }
    }
}
