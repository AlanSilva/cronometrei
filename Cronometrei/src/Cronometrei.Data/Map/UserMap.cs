﻿using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cronometrei.Data.Map
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.Login)
                   .HasColumnType("varchar(10)")
                   .IsRequired();

            builder.Property(user => user.Password)
                   .HasColumnType("varchar(20)")
                   .IsRequired();

            builder.Property(user => user.Role)
                   .HasColumnType("varchar(20)")
                   .IsRequired();

            builder.HasIndex(user => user.Login)
                   .IsUnique();
        }
    }
}
