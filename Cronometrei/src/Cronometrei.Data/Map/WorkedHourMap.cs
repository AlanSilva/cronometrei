﻿using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cronometrei.Data.Map
{
    public class WorkedHourMap : IEntityTypeConfiguration<WorkedHour>
    {
        public void Configure(EntityTypeBuilder<WorkedHour> builder)
        {
            builder.HasOne(workedHour => workedHour.Employee)
                   .WithMany(employee => employee.WorkedHours)
                   .HasForeignKey(workedHour => workedHour.EmployeeId);

            builder.HasOne(workedHour => workedHour.Project)
                   .WithMany(project => project.WorkedHours)
                   .HasForeignKey(workedHour => workedHour.ProjectId);
        }
    }
}
