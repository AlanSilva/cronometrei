﻿using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cronometrei.Data.Map
{
    public class ProjectMap : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.Property(project => project.Name)
                   .HasColumnType("varchar(50)")
                   .IsRequired();

            builder.Property(project => project.Description)
                   .IsRequired();
        }
    }
}
