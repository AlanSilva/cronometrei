﻿using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cronometrei.Data.Map
{
    public class EmployeeTeamMap : IEntityTypeConfiguration<EmployeeTeam>
    {
        public void Configure(EntityTypeBuilder<EmployeeTeam> builder)
        {
            builder.HasKey(employeeTeam => new { employeeTeam.EmployeeId, employeeTeam.TeamId });

            builder.HasOne(employeeTeam => employeeTeam.Employee)
                   .WithMany(employee => employee.EmployeeTeams)
                   .HasForeignKey(employeeTeam => employeeTeam.EmployeeId);

            builder.HasOne(employeeTeam => employeeTeam.Team)
                   .WithMany(team => team.EmployeeTeams)
                   .HasForeignKey(employeeTeam => employeeTeam.TeamId);
        }
    }
}
