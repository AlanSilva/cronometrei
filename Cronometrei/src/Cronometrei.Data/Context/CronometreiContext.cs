﻿using Cronometrei.Data.Map;
using Cronometrei.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Cronometrei.Data.Context
{
    public class CronometreiContext : DbContext
    {
        public CronometreiContext(DbContextOptions<CronometreiContext> options) : base(options)
        {
            //
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<EmployeeTeam> EmployeeTeams { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<WorkedHour> WorkedHours { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var relationships = modelBuilder.Model.GetEntityTypes()
                                                  .SelectMany(e => e.GetForeignKeys());

            foreach (var relationship in relationships)
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            var properties = modelBuilder.Model.GetEntityTypes()
                                               .SelectMany(t => t.GetProperties())
                                               .Where(p => p.ClrType == typeof(string));

            foreach (var property in properties)
                property.SetColumnType("varchar(250)");

            // Map config
            modelBuilder.ApplyConfiguration(new EmployeeMap());

            modelBuilder.ApplyConfiguration(new EmployeeTeamMap());

            modelBuilder.ApplyConfiguration(new ProjectMap());

            modelBuilder.ApplyConfiguration(new TeamMap());

            modelBuilder.ApplyConfiguration(new UserMap());

            modelBuilder.ApplyConfiguration(new WorkedHourMap());
        }
    }
}
