﻿using Cronometrei.Support.Dtos.UserDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.UserValidators
{
    public class PostUserAuthenticationDtoValidator : AbstractValidator<PostUserAuthenticationDto>
    {
        public PostUserAuthenticationDtoValidator()
        {
            RuleFor(team => team.Login).NotEmpty().WithMessage("Por favor, informe um login válido.");
            RuleFor(team => team.Login).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(team => team.Login).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(team => team.Password).NotEmpty().WithMessage("Por favor, informe uma senha válida.");
            RuleFor(team => team.Password).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(team => team.Password).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
        }
    }
}
