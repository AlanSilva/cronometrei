﻿using Cronometrei.Support.Dtos.ProjectDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.ProjectValidators
{
    public class PostLinkTeamToProjectDtoValidator : AbstractValidator<PostLinkTeamToProjectDto>
    {
        public PostLinkTeamToProjectDtoValidator()
        {
            RuleFor(linkTeamToProject => linkTeamToProject.ProjectId).NotEmpty().WithMessage("Por favor, informe um identificador válido para o ID do projeto.");
            RuleFor(linkTeamToProject => linkTeamToProject.TeamId).NotEmpty().WithMessage("Por favor, informe um identificador válido para o ID da equipe.");
        }
    }
}
