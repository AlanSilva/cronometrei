﻿using Cronometrei.Support.Dtos.ProjectDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.ProjectValidators
{
    public class PostProjectDtoValidator : AbstractValidator<PostProjectDto>
    {
        public PostProjectDtoValidator()
        {
            RuleFor(project => project.Name).NotEmpty().WithMessage("Por favor, informe um nome válido.");
            RuleFor(project => project.Name).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 50 caracteres.");
            RuleFor(project => project.Name).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 50 caracteres.");
            RuleFor(project => project.Description).NotEmpty().WithMessage("Por favor, informe uma descrição válida.");
            RuleFor(project => project.Description).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(project => project.Description).MaximumLength(250).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
        }
    }
}
