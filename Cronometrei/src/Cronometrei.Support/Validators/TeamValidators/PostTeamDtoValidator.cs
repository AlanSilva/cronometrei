﻿using Cronometrei.Support.Dtos.TeamDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.TeamValidators
{
    public class PostTeamDtoValidator : AbstractValidator<PostTeamDto>
    {
        public PostTeamDtoValidator()
        {
            RuleFor(team => team.Name).NotEmpty().WithMessage("Por favor, informe um nome válido.");
            RuleFor(team => team.Name).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 50 caracteres.");
            RuleFor(team => team.Name).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 50 caracteres.");
        }
    }
}
