﻿using Cronometrei.Support.Dtos.EmployeeTeamDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.EmployeeTeamValidators
{
    public class PostEmployeeTeamDtoValidator : AbstractValidator<PostEmployeeTeamDto>
    {
        public PostEmployeeTeamDtoValidator()
        {
            RuleFor(collaborate => collaborate.EmployeeId).NotEmpty().WithMessage("Por favor, informe um ID válido para o colaborador.");
            RuleFor(collaborate => collaborate.TeamId).NotEmpty().WithMessage("Por favor, informe um ID válido para o time.");
        }
    }
}
