﻿using Cronometrei.Support.Dtos.WorkedHourDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.WorkedHourValidators
{
    public class PostWorkedHourDtoValidator : AbstractValidator<PostWorkedHourDto>
    {
        public PostWorkedHourDtoValidator()
        {
            RuleFor(workedHour => workedHour.EmployeeId).NotEmpty().WithMessage("Por favor, informe um ID válido para o colaborador.");
            RuleFor(workedHour => workedHour.ProjectId).NotEmpty().WithMessage("Por favor, informe um ID válido para o projeto.");
            RuleFor(workedHour => workedHour.Hours).GreaterThan(-1).WithMessage("Não é possível informar valores negativos para as horas.");
            RuleFor(workedHour => workedHour.Minutes).GreaterThan(-1).WithMessage("Não é possível informar valores negativos para os minutos.");
        }
    }
}
