﻿using Cronometrei.Support.Dtos.CollaboratorDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.CollaboratorValidators
{
    public class PostCollaboratorDtoValidator : AbstractValidator<PostCollaboratorDto>
    {
        public PostCollaboratorDtoValidator()
        {
            RuleFor(collaborate => collaborate.Name).NotEmpty().WithMessage("Por favor, informe um nome válido.");
            RuleFor(collaborate => collaborate.Name).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(collaborate => collaborate.Name).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(collaborate => collaborate.Registration).NotEmpty().WithMessage("Por favor, informe uma mátricula válida.");
            RuleFor(collaborate => collaborate.Registration).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(collaborate => collaborate.Registration).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(collaborate => collaborate.BirthDate).NotEmpty().WithMessage("Por favor, informe uma data de nascimento válida.");
            RuleFor(collaborate => collaborate.Login).NotEmpty().WithMessage("Por favor, informe um login válido.");
            RuleFor(collaborate => collaborate.Login).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(collaborate => collaborate.Login).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(collaborate => collaborate.Password).NotEmpty().WithMessage("Por favor, informe uma senha válida.");
            RuleFor(collaborate => collaborate.Password).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(collaborate => collaborate.Password).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
        }
    }
}
