﻿using Cronometrei.Support.Dtos.ManagerDtos;
using FluentValidation;

namespace Cronometrei.Support.Validators.ManagerValidators
{
    public class PostManagerDtoValidator : AbstractValidator<PostManagerDto>
    {
        public PostManagerDtoValidator()
        {
            RuleFor(manager => manager.Name).NotEmpty().WithMessage("Por favor, informe um nome válido.");
            RuleFor(manager => manager.Name).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(manager => manager.Name).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(manager => manager.Registration).NotEmpty().WithMessage("Por favor, informe uma mátricula válida.");
            RuleFor(manager => manager.Registration).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(manager => manager.Registration).MaximumLength(50).WithMessage("Por favor, informe um valor entre 1 e 250 caracteres.");
            RuleFor(manager => manager.BirthDate).NotEmpty().WithMessage("Por favor, informe uma data de nascimento válida.");
            RuleFor(manager => manager.Login).NotEmpty().WithMessage("Por favor, informe um login válido.");
            RuleFor(manager => manager.Login).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(manager => manager.Login).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(manager => manager.Password).NotEmpty().WithMessage("Por favor, informe uma senha válida.");
            RuleFor(manager => manager.Password).MinimumLength(1).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
            RuleFor(manager => manager.Password).MaximumLength(20).WithMessage("Por favor, informe um valor entre 1 e 20 caracteres.");
        }
    }
}
