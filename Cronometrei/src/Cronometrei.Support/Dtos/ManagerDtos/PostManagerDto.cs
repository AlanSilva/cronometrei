﻿using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.EmployeeDtos;
using System;

namespace Cronometrei.Support.Dtos.ManagerDtos
{
    public class PostManagerDto : PostEmployeeDto
    {
        public static implicit operator Employee(PostManagerDto managerDto)
        {
            var id = Guid.NewGuid();

            var user = new User(id, login: managerDto.Login.ToLower(), password: managerDto.Password, role: "Manager");

            var employee = new Employee(id, name: managerDto.Name, registration: managerDto.Registration, birthDate: managerDto.BirthDate, user);

            return employee;
        }
    }
}
