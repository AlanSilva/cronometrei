﻿using Cronometrei.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.ProjectDtos
{
    public class PostProjectDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public static implicit operator Project(PostProjectDto projectDto)
        {
            var project = new Project(name: projectDto.Name, description: projectDto.Description);

            return project;
        }
    }
}
