﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.ProjectDtos
{
    public class PostLinkTeamToProjectDto
    {
        [Required]
        public Guid ProjectId { get; set; }

        [Required]
        public Guid TeamId { get; set; }
    }
}
