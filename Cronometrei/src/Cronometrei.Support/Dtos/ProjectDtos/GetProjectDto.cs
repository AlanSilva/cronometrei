﻿using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.TeamDtos;
using Cronometrei.Support.Helpers;
using System;

namespace Cronometrei.Support.Dtos.ProjectDtos
{
    public class GetProjectDto
    {
        public Guid Id { get; set; }

        public Guid? TeamId { get; set; }

        public TeamDto Team { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Status { get; set; }

        public DateTime CreatedAt { get; set; }

        public TimeSpan TotalAmountOfHours { get; set; }

        public static implicit operator GetProjectDto(Project project)
        {
            TimeSpan totalAmountOfHours = WorkedHourHelper.GetTotalAmountOfHours(project.WorkedHours);

            var projectDto = new GetProjectDto
            {
                Id = project.Id,
                TeamId = project.TeamId,
                Name = project.Name,
                Description = project.Description,
                Status = project.Status,
                CreatedAt = project.CreatedAt,
                TotalAmountOfHours = totalAmountOfHours
            };

            if (project.Team != null)
            {
                TeamDto teamDto = project.Team;

                projectDto.Team = teamDto;
            }

            return projectDto;
        }

    }
}
