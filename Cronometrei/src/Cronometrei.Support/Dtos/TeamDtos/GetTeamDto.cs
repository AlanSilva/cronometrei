﻿using Cronometrei.Domain.Models;
using Cronometrei.Support.Helpers;
using System;

namespace Cronometrei.Support.Dtos.TeamDtos
{
    public class GetTeamDto : AbstractTeamDto
    {
        public TimeSpan TotalAmountOfHours { get; set; }

        public static implicit operator GetTeamDto(Team team)
        {
            TimeSpan totalAmountOfHours = WorkedHourHelper.GetTotalAmpuntOfHoursUsedInTheTeam(team.EmployeeTeams);

            var teamDto = new GetTeamDto
            {
                Id = team.Id,
                Name = team.Name,
                Status = team.Status,
                TotalAmountOfHours = totalAmountOfHours
            };

            return teamDto;
        }
    }
}
