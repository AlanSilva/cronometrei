﻿using System;

namespace Cronometrei.Support.Dtos.TeamDtos
{
    public abstract class AbstractTeamDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool Status { get; set; }
    }
}
