﻿using Cronometrei.Domain.Models;

namespace Cronometrei.Support.Dtos.TeamDtos
{
    public class TeamDto : AbstractTeamDto
    {
        public static implicit operator TeamDto(Team team)
        {
            var teamDto = new TeamDto
            {
                Id = team.Id,
                Name = team.Name,
                Status = team.Status
            };

            return teamDto;
        }
    }
}
