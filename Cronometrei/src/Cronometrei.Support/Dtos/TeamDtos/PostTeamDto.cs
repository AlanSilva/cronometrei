﻿using Cronometrei.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.TeamDtos
{
    public class PostTeamDto
    {
        [Required]
        public string Name { get; set; }

        public static implicit operator Team(PostTeamDto teamDto)
        {
            var team = new Team(name: teamDto.Name);

            return team;
        }
    }
}
