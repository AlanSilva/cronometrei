﻿using Cronometrei.Domain.Models;
using Cronometrei.Support.Dtos.EmployeeDtos;
using System;

namespace Cronometrei.Support.Dtos.CollaboratorDtos
{
    public class PostCollaboratorDto : PostEmployeeDto
    {
        public static implicit operator Employee(PostCollaboratorDto collaborateDto)
        {
            var id = Guid.NewGuid();

            var user = new User(id, login: collaborateDto.Login.ToLower(), password: collaborateDto.Password, role: "Collaborator");

            var employee = new Employee(id, name: collaborateDto.Name, registration: collaborateDto.Registration, birthDate: collaborateDto.BirthDate, user);

            return employee;
        }
    }
}
