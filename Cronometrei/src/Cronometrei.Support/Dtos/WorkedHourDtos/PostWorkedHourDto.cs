﻿using Cronometrei.Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.WorkedHourDtos
{
    public class PostWorkedHourDto
    {
        [Required]
        public Guid EmployeeId { get; set; }

        [Required]
        public Guid ProjectId { get; set; }

        [Required]
        public int Hours { get; set; }

        [Required]
        public int Minutes { get; set; }

        public static implicit operator WorkedHour(PostWorkedHourDto workedHourDto)
        {
            var workedHour = new WorkedHour(employeeId: workedHourDto.EmployeeId, projectId: workedHourDto.ProjectId, hours: workedHourDto.Hours, minutes: workedHourDto.Minutes);

            return workedHour;
        }
    }
}
