﻿using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.UserDtos
{
    public class PostUserAuthenticationDto
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
