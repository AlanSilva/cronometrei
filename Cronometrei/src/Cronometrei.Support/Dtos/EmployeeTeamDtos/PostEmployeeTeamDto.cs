﻿using Cronometrei.Domain.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.EmployeeTeamDtos
{
    public class PostEmployeeTeamDto
    {
        [Required]
        public Guid EmployeeId { get; set; }

        [Required]
        public Guid TeamId { get; set; }

        public static implicit operator EmployeeTeam(PostEmployeeTeamDto employeeTeamDto)
        {
            var employeeTeam = new EmployeeTeam(employeeId: employeeTeamDto.EmployeeId, teamId: employeeTeamDto.TeamId);

            return employeeTeam;
        }
    }
}
