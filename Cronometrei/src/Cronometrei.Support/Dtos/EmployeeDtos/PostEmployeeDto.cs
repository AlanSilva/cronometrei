﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cronometrei.Support.Dtos.EmployeeDtos
{
    public abstract class PostEmployeeDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Registration { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
