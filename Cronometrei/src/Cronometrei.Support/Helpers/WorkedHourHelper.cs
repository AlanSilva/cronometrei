﻿using Cronometrei.Domain.Models;
using System;
using System.Collections.Generic;

namespace Cronometrei.Support.Helpers
{
    public static class WorkedHourHelper
    {
        public static TimeSpan GetTotalAmountOfHours(List<WorkedHour> workedHours)
        {
            TimeSpan totalAmountOfHours = new TimeSpan(0, 0, 0);

            workedHours.ForEach(workedHour =>
            {
                TimeSpan hours = new TimeSpan(workedHour.Hours, workedHour.Minutes, 0);

                totalAmountOfHours = totalAmountOfHours.Add(hours);
            });

            return totalAmountOfHours;
        }

        public static TimeSpan GetTotalAmpuntOfHoursUsedInTheTeam(List<EmployeeTeam> employeeTeams)
        {
            TimeSpan totalAmountOfHours = new TimeSpan(0, 0, 0);

            employeeTeams.ForEach(employeeTeam =>
            {
                TimeSpan totalEmployeeTeamsHours = GetTotalAmountOfHours(employeeTeam.Employee.WorkedHours);

                totalAmountOfHours = totalAmountOfHours.Add(totalEmployeeTeamsHours);
            });

            return totalAmountOfHours;
        }
    }
}
