﻿using Cronometrei.Histories.TeamHistory;
using Cronometrei.Support.Dtos.TeamDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class EquipesController : ControllerBase
    {
        /// <summary>
        /// Rota para cadastrar equipe
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostTeam([FromServices] AddTeam addTeam, PostTeamDto teamDto)
        {
            if (ModelState.IsValid)
            {
                await addTeam.Run(teamDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }

        /// <summary>
        /// Rota para visualizar detalhes de uma equipe e suas horas utilizadas
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Details([FromServices] GetTeamById getTeamById, Guid id)
        {
            try
            {
                var teamDto = await getTeamById.Run(id);

                if (teamDto is null)
                    return BadRequest($"Não foi encontado nenhuma equipe com identificador { id } na base de dados.");

                return new ObjectResult(teamDto);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Rota para listar os times e suas horas trabalhadas.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllTeams([FromServices] ListTeams listTeams)
        {
            try
            {
                var projectsDto = await listTeams.Run();

                return new ObjectResult(projectsDto);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}
