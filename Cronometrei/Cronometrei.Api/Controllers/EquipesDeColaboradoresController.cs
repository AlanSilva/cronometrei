﻿using Cronometrei.Histories.EmployeeHistory.Collaborator;
using Cronometrei.Histories.EmployeeTeamHistory;
using Cronometrei.Histories.TeamHistory;
using Cronometrei.Support.Dtos.EmployeeTeamDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class EquipesDeColaboradoresController : ControllerBase
    {
        /// <summary>
        /// Rota para vincular colaborador a uma equipe
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostCollaboratorTeams(
            [FromServices] CheckIfTheCollaboratorExists checkIfTheCollaboratorExists,
            [FromServices] CheckIfTheTeamExists checkIfTheTeamExists,
            [FromServices] CheckIfTheCollaboratorIsAlreadyOnTheTeam checkIfTheCollaboratorIsAlreadyOnTheTeam,
            [FromServices] AddCollaboratorToTeam addCollaboratorToTeam,
            PostEmployeeTeamDto employeeTeamDto)
        {
            if (ModelState.IsValid)
            {
                bool collaboratorExists = await checkIfTheCollaboratorExists.Run(employeeId: employeeTeamDto.EmployeeId);

                if (!collaboratorExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há colaborador com ID { employeeTeamDto.EmployeeId } cadastrado na base de dados."
                    });

                bool teamExists = await checkIfTheTeamExists.Run(teamId: employeeTeamDto.TeamId);

                if (!teamExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há equipe com ID { employeeTeamDto.TeamId } cadastrado na base de dados."
                    });

                bool collaboratorAlreadyOnTheTeam = await checkIfTheCollaboratorIsAlreadyOnTheTeam.Run(employeeId: employeeTeamDto.EmployeeId, teamId: employeeTeamDto.TeamId);

                if (collaboratorAlreadyOnTheTeam)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"O colaborador já possui vínculo com essa equipe."
                    });

                await addCollaboratorToTeam.Run(employeeTeamDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }
    }
}
