﻿using Cronometrei.Api.Services;
using Cronometrei.Histories.UserHistory;
using Cronometrei.Support.Dtos.UserDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutenticacaoController : ControllerBase
    {
        /// <summary>
        /// Rota para autenticação de usuário.
        /// </summary>
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromServices] GetUser getUser, PostUserAuthenticationDto userAuthenticationDto)
        {
            var user = await getUser.Run(login: userAuthenticationDto.Login, password: userAuthenticationDto.Password);

            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            var token = TokenService.GenerateToken(user);

            user.SetPasswordToEmpty();

            return new
            {
                user,
                token
            };
        }
    }
}
