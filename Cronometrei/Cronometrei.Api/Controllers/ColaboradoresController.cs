﻿using Cronometrei.Histories.EmployeeHistory.Collaborator;
using Cronometrei.Histories.UserHistory;
using Cronometrei.Support.Dtos.CollaboratorDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class ColaboradoresController : ControllerBase
    {
        /// <summary>
        /// Rota para cadastrar colaborador
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostCollaborator([FromServices] CheckIfUserAlreadyExistsByLogin checkIfUserAlreadyExistsByLogin, [FromServices] AddCollaborator addCollaborator, PostCollaboratorDto collaboratorDto)
        {
            if (ModelState.IsValid)
            {
                bool userAlreadExists = await checkIfUserAlreadyExistsByLogin.Run(login: collaboratorDto.Login);

                if (userAlreadExists)
                    return BadRequest($"Já existe um usuário com o login { collaboratorDto.Login } cadastrado na base de dados.");

                await addCollaborator.Run(collaboratorDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }
    }
}
