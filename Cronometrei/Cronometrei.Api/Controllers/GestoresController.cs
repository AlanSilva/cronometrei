﻿using Cronometrei.Histories.EmployeeHistory.Manager;
using Cronometrei.Histories.UserHistory;
using Cronometrei.Support.Dtos.ManagerDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class GestoresController : ControllerBase
    {
        /// <summary>
        /// Rota para cadastrar gestor
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostManager([FromServices] CheckIfUserAlreadyExistsByLogin checkIfUserAlreadyExistsByLogin, [FromServices] AddManager addManager, PostManagerDto managerDto)
        {
            if (ModelState.IsValid)
            {
                bool userAlreadExists = await checkIfUserAlreadyExistsByLogin.Run(login: managerDto.Login);

                if (userAlreadExists)
                    return BadRequest($"Já existe um usuário com o login { managerDto.Login } cadastrado na base de dados.");

                await addManager.Run(managerDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }
    }
}
