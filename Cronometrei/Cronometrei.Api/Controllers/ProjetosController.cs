﻿using Cronometrei.Histories.ProjectHistory;
using Cronometrei.Histories.TeamHistory;
using Cronometrei.Support.Dtos.ProjectDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Manager")]
    public class ProjetosController : ControllerBase
    {
        /// <summary>
        /// Rota para cadastrar projeto
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostProject([FromServices] AddProject addProject, PostProjectDto projectDto)
        {
            if (ModelState.IsValid)
            {
                await addProject.Run(projectDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }

        /// <summary>
        /// Rota para vincular equipe ao projeto
        /// </summary>
        [HttpPost("VincularEquipeAoProjeto")]
        public async Task<IActionResult> VincularEquipeAoProjeto(
            [FromServices] CheckIfTheProjectExists checkIfTheProjectExists,
            [FromServices] CheckIfTheTeamExists checkIfTheTeamExists,
            [FromServices] LinkTeamToProject linkTeamToProject,
            PostLinkTeamToProjectDto linkTeamToProjectDto)
        {
            if (ModelState.IsValid)
            {
                bool projectExists = await checkIfTheProjectExists.Run(projectId: linkTeamToProjectDto.ProjectId);

                if (!projectExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há projeto com ID { linkTeamToProjectDto.ProjectId } cadastrado na base de dados."
                    });

                bool teamExists = await checkIfTheTeamExists.Run(linkTeamToProjectDto.TeamId);

                if (!teamExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há equipe com ID { linkTeamToProjectDto.TeamId } cadastrado na base de dados."
                    });

                await linkTeamToProject.Run(linkTeamToProjectDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }

        /// <summary>
        /// Rota para listar os projetos e suas horas trabalhadas.
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllProjects([FromServices] ListProjects listProjects)
        {
            try
            {
                var projectsDto = await listProjects.Run();

                return new ObjectResult(projectsDto);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Rota para visualizar detalhes de um projeto e suas horas trabalhadas
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> Details([FromServices] GetProjectById getProjectById, Guid id)
        {
            try
            {
                var projectDto = await getProjectById.Run(id);

                if (projectDto is null)
                    return BadRequest($"Não foi encontado nenhum projeto com identificador { id } na base de dados.");

                return new ObjectResult(projectDto);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}
