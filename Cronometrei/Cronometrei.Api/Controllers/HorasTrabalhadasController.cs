﻿using Cronometrei.Histories.EmployeeHistory.Collaborator;
using Cronometrei.Histories.ProjectHistory;
using Cronometrei.Histories.WorkedHourHistory;
using Cronometrei.Support.Dtos.WorkedHourDtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cronometrei.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Collaborator")]
    public class HorasTrabalhadasController : ControllerBase
    {
        /// <summary>
        /// Rota para os colaboradores registrarem suas horas diárias trabalhada nos projetos.
        /// Exemplo de entrada permitida: 6:15 | 6 (convertido automaticamente para horas) | 0:15 | 6h15m | 6h | 15m
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> PostWorkedHours(
            [FromServices] CheckIfTheCollaboratorExists checkIfTheCollaboratorExists,
            [FromServices] CheckIfTheProjectExists checkIfTheProjectExists,
            [FromServices] RegisterHoursWorked registerHoursWorked,
            PostWorkedHourDto workedHourDto)
        {
            if (ModelState.IsValid)
            {
                bool collaboratorExists = await checkIfTheCollaboratorExists.Run(employeeId: workedHourDto.EmployeeId);

                if (!collaboratorExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há colaborador com ID { workedHourDto.EmployeeId } cadastrado na base de dados."
                    });

                bool projectExists = await checkIfTheProjectExists.Run(projectId: workedHourDto.ProjectId);

                if (!projectExists)
                    return BadRequest(new
                    {
                        data = ModelState,
                        message = $"Não há projeto com ID { workedHourDto.ProjectId } cadastrado na base de dados."
                    });

                await registerHoursWorked.Run(workedHourDto);

                return Ok();
            }

            return BadRequest(new { data = ModelState });
        }
    }
}
